# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile-kivy.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from typing import Any

from kivy.app import App
from kivy.properties import ListProperty, ColorProperty, ObjectProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.screenmanager import Screen
from kivy.utils import get_color_from_hex

from turntile.networking.util import ScreenType, MessageType
from turntile.networking.network import network
from turntile.parsing.config import TileData
from turntile.state import state
from turntile_kivy.util import texture_from_bytes, SlotLayout


class Slot(RecycleDataViewBehavior, ButtonBehavior, SlotLayout):
    tiles = ListProperty()

    index = 0

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        app: Any = App.get_running_app()
        self.game_screen: GameScreen = app.screen_manager.get_screen(ScreenType.GAME.name)

    def refresh_view_attrs(self, rv: Any, index: int, data: dict[str, Any]) -> None:
        self.index = index

        self.clear_widgets()
        for tile in data["tiles"]:
            self.add_tile(tile)

        super().refresh_view_attrs(rv, index, data)

    def on_press(self) -> None:
        if request := state.board.handle_press(self.index):
            network.send(MessageType.REQUEST_ACTS, request)

    def add_tile(self, tile: TileData) -> None:
        self.add_widget(
            Image(texture=texture_from_bytes(tile.data), allow_stretch=True, keep_ratio=True)
            if not tile.single_color else
            Image(color=get_color_from_hex(tile.name))
        )


class Board(RecycleView):
    color = ColorProperty((0, 0, 0, 1))

    def setup_board(self, slots: list[list[TileData]], color: str) -> None:
        data = []
        for tiles in slots:
            data.append({"tiles": tiles.copy()})

        self.data = data
        self.color = get_color_from_hex(color)

    def update_board(self, tile: TileData, slot_index: int, level: int, replace: bool) -> None:
        slot = self.data[slot_index].copy()
        if replace:
            if tile.name == "":
                slot["tiles"].pop(level)
            else:
                slot["tiles"][level] = tile
        else:
            slot["tiles"].insert(level, tile)
        self.data[slot_index] = slot


class GameArea(GridLayout):
    pass


class GameScreen(Screen):
    game_area: GameArea = ObjectProperty(None)

    board: Board = ObjectProperty(None)
    slot: Slot = ObjectProperty(None)

    def init(self) -> None:
        self.game_area.clear_widgets()

        if state.board:
            board = Board()
            state.board.init_board_ui(board)
            self.game_area.add_widget(board)
