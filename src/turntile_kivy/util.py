# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile-kivy.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import io
from typing import Any

from kivy.core.text import Label
from kivy.uix.layout import Layout
from kivy.uix.popup import Popup
from kivy.core.image import Image as CoreImage


class MessagePopup(Popup):
    def __init__(self, title: str, msg: str) -> None:
        super().__init__(title=title)

        message_label: Label = self.ids.message_label
        message_label.text = msg


def texture_from_bytes(image_bytes: bytes) -> Any:
    data = io.BytesIO(image_bytes)
    return CoreImage(data, ext="png").texture


class SlotLayout(Layout):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        fbind = self.fbind
        update = self._trigger_layout
        fbind('children', update)
        fbind('parent', update)
        fbind('size', update)
        fbind('pos', update)

    def do_layout(self, *args: Any) -> None:
        _x, _y = self.pos
        width = self.width
        height = self.height

        for c in self.children:
            cw, ch = c.size
            shw, shh = c.size_hint

            if shw is not None:
                cw = shw * width

            if shh is not None:
                ch = shh * height

            c.pos = _x + (width - cw) / 2, _y + (height - ch) / 2
            c.size = cw, ch



