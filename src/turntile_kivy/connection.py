# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile-kivy.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen

from turntile.networking.network import network
from turntile_kivy.util import MessagePopup


class ConnectionScreen(Screen):
    name_input = ObjectProperty("")

    def _address(self) -> str:
        address: str = self.ids.address_input.text
        hint_address: str = self.ids.address_input.hint_text
        return address if address else hint_address

    def _port(self) -> int:
        port = self.ids.port_input.text
        hint_port = int(self.ids.port_input.hint_text)
        return int(port) if port else hint_port

    def connect(self) -> None:
        if len(self.ids.name_input.text) == 0:
            return

        error_msg = network.connect_to_server(
            self._address(),
            self._port(),
            self.ids.name_input.text
        )

        if error_msg:
            MessagePopup("Error", error_msg).open()

