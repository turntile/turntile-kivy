# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile-kivy.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# from kivy.config import Config
# Config.set("kivy", "log_level", "warning")
# Config.write()
#
from typing import Any

from kivy.app import App
from kivy.clock import mainthread, Clock
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, RiseInTransition

from turntile.networking.util import BaseInterfaceHandler, ScreenType, MessageType
from turntile.networking.network import network
from turntile.parsing.config import Config
from turntile.state import state
from turntile_kivy.connection import ConnectionScreen
from turntile_kivy.game import GameScreen
from turntile_kivy.lobby import LobbyScreen
from turntile_kivy.util import MessagePopup


class InterfaceHandler(BaseInterfaceHandler):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.app = KivyInterface(self)

    def start_client(self) -> None:
        if self.app.handler.auto_connect_address != "" and self.app.handler.default_player_name != "":
            Clock.schedule_once(lambda _: self.app.screen_manager.get_screen(ScreenType.CONNECTION.name).connect())

        self.app.run()

    @mainthread
    def set_screen(self, screen_type: ScreenType) -> None:
        self.app.screen_manager.current = screen_type.name

        if (self.app.handler.auto_player_assign and screen_type == ScreenType.LOBBY and
                (assignable := state.client_state.current_assignable_tiles(""))):

            network.send(MessageType.REQUEST_CLIENT_UPDATE,
                         {"tile": assignable[0],
                          "ready": True})

    @mainthread
    def set_clients(self, clients: dict[str, dict[str, Any]]) -> None:
        state.set_clients(clients)

        lobby_screen = self.app.screen_manager.get_screen(ScreenType.LOBBY.name)
        lobby_screen.lobby_rc.data = [
            {"name": info["name"],
             "tile": info["tile"],
             "is_control_client": state.player.address == address,
             "ready": info["ready"]} for address, info in clients.items()
        ]

    @mainthread
    def show_message(self, message: str) -> None:
        MessagePopup("Info", message).open()

    @mainthread
    def init_config(self, configuration: dict[str, Any]) -> None:
        connection_screen = self.app.screen_manager.get_screen(ScreenType.CONNECTION.name)

        state.set_config(Config(configuration), connection_screen.name_input.text)

        lobby_screen = self.app.screen_manager.get_screen(ScreenType.LOBBY.name)
        lobby_screen.game_name = state.config.name
        lobby_screen.game_description = state.config.description

        game_screen = self.app.screen_manager.get_screen(ScreenType.GAME.name)
        game_screen.init()

    @mainthread
    def open_game(self, player_name: str) -> None:
        state.open_game(player_name)
        self.set_screen(ScreenType.GAME)

    @mainthread
    def do_acts(self, acts_raw: list[list[Any]]) -> None:
        state.board.do_acts(acts_raw)

    @mainthread
    def next_turn(self, ply: int) -> None:
        pass  # state.next_turn()  # todo: handle next turn


def handle_window_close(*args: Any, **kwargs: Any) -> None:
    if len(network.connections) > 0:
        network.send(MessageType.DISCONNECT)


class KivyInterface(App):
    def __init__(self, handler: InterfaceHandler) -> None:
        super().__init__()
        self.handler = handler
        self.screen_manager = ScreenManager(transition=RiseInTransition())
        Window.bind(on_request_close=handle_window_close)

    def build(self) -> ScreenManager:
        self.screen_manager.add_widget(ConnectionScreen())
        self.screen_manager.add_widget(LobbyScreen())
        self.screen_manager.add_widget(GameScreen())

        self.title = ""

        return self.screen_manager
