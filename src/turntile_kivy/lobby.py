# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile-kivy.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from typing import Any

from kivy.properties import ObjectProperty, StringProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.screenmanager import Screen

from turntile.networking.network import network
from turntile.networking.util import MessageType
from turntile.state import state


class TileSelectButton(Button):
    pass


class TileDropDown(DropDown):
    def open_dropdown(self, button: TileSelectButton) -> None:
        self.clear_widgets()

        if assignable_tiles := state.client_state.current_assignable_tiles(button.text):
            for tile in assignable_tiles:
                btn = TileSelectButton(text=tile)
                btn.bind(on_release=lambda b: self.select(b.text))
                self.add_widget(btn)

            self.open(button)


class PlayerRow(RecycleDataViewBehavior, BoxLayout):
    name = StringProperty("")
    tile = StringProperty("")
    ready = BooleanProperty(False)
    is_control_client = BooleanProperty(False)

    index = 0

    def refresh_view_attrs(self, rv: Any, index: int, data: dict[str, Any]) -> None:
        self.index = index
        super().refresh_view_attrs(rv, index, data)

    def store_ready_state(self) -> None:
        network.send(MessageType.REQUEST_CLIENT_UPDATE, {"ready": self.ready})

    def store_assignation(self, selected: str) -> None:
        self.tile = selected
        if selected == "":
            self.ready = False
        network.send(MessageType.REQUEST_CLIENT_UPDATE, {"tile": self.tile, "ready": self.ready})


class LobbyScreen(Screen):
    game_name = StringProperty("")
    game_description = StringProperty("")

    lobby_rc = ObjectProperty(None)

    @staticmethod
    def start_game() -> None:
        network.send(MessageType.REQUEST_START_GAME)
